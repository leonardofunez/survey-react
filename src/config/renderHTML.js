import React from 'react'

const renderHTML = (rawHTML) => {
  return React.createElement("span", { dangerouslySetInnerHTML: { __html: rawHTML } })
}

export default renderHTML