import React from 'react'
import PropTypes from 'prop-types'
import './ChartResults.scss'

const ChartResults = (props) => {
  const rightStyle = {
    width: `${props.right}0%`
  };
  
  const wrongStyle = {
    width: `${props.total - props.right}0%`
  };

  return(
    <div className='chart-results w-100'>
      <div className='chart-results__bars is-flex flex-justify-center b-radius-4 overflow-h'>
        <div className='chart-results__item bg-color-blue' style={rightStyle}></div>
        <div className='chart-results__item bg-color-red' style={wrongStyle}></div>
      </div>

      <div className='chart-results__scored is-flex flex-column f-16 f-weight-600 align-center'>
        You scored<br />
        <span className='f-20'>{props.right} / {props.total}</span>
      </div>
    </div>
  )
}

ChartResults.propTypes = {
  right : PropTypes.number.isRequired,
  total : PropTypes.number.isRequired
}

export default ChartResults