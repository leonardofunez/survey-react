import React from 'react'
import PropTypes from 'prop-types'
import './Answer.scss'

const Answer = props => {
  return(
    <div className='answer bg-color-white b-radius-4 p-20 is-flex flex-between flex-align-center'>
      <div className='answer__check rounded' data-ok={'true' ? props.is_ok : 'false'}></div>
      <div className='answer__info'>
        <span className='answer__category color-dark-gray f-12'>{props.category}</span>
        <h2 className='answer__question f-16 f-weight-500'>{props.question}</h2>
      </div>
    </div>
  )
}

Answer.propTypes = {
  is_ok : PropTypes.bool.isRequired,
  category : PropTypes.string.isRequired
}

export default Answer