import React from 'react'
import { NavLink } from 'react-router-dom'
import './Header.scss'
import profilePhoto  from '../../assets/img/profile.jpg'

const Header = () => {
  const styleProfile = {
    background: `url(${profilePhoto}) no-repeat center / cover`
  }

  return(
    <header className='main-header bg-color-white b-shadow p-20 is-flex flex-align-center flex-between is-fixed w-100 to-top to-left z-1'>
      <NavLink to='/' className='logo'></NavLink>

      <div className='profile is-flex flex-align-center'>
        <span className='profile__username f-14 f-weight-600'>Leonardo Funez</span>
        <div className='profile__photo bg-color-gray rounded' style={styleProfile}></div>
      </div>
    </header>
  )
}

export default Header