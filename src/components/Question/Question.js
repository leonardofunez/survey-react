import React from 'react'
import PropTypes from 'prop-types'
import './Question.scss'

import store from '../../redux/store'
import { ANSWERS } from '../../redux/creators'

const Question = props => {
  const saveVote = (value) => {
    // Creating an object with the question answered
    const question = {index: props.questionIndex, answer: value}

    // Getting current Store
    let userAnswers = store.getState().answers
    
    userAnswers = [...userAnswers, question]

    // Saving in store
    store.dispatch(ANSWERS(userAnswers))

    // Sending the index to parent
    if(props.isAnswered) props.isAnswered(props.questionIndex)
  }

  return(
    <div className='question align-center is-flex flex-column flex-justify-center flex-between is-relative' data-visible={props.visible}>
      <h2 className='question__category f-weight-700 f-24'>{props.category}</h2>
      
      <p className='question__description f-16'>{props.description}</p>
      
      <div className='like-buttons is-flex flex-align-center flex-between'>
        <button className='like-button like-button__dislike' onClick={() => saveVote(false)}>
          <span className='like-button__icon'></span>
        </button>
        <button className='like-button' onClick={() => saveVote(true)}>
          <span className='like-button__icon'></span>
        </button>
      </div>
    </div>
  )
}

Question.propTypes = {
  questionIndex : PropTypes.number.isRequired,
  visible : PropTypes.bool.isRequired,
  category : PropTypes.string.isRequired,
  description: PropTypes.object.isRequired
}

export default Question