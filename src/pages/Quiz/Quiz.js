import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'

import renderHTML from '../../config/renderHTML'
import APISurvey from '../../config/api'
import store from '../../redux/store'
import { IS_LOADING, ORG_QUESTIONS } from '../../redux/creators'

import Question from '../../components/Question/Question'

const Survey = (props) => {
  const [questions, setQuestions] = useState([]),
        [currentQuestion, setCurrentQuestion] = useState(1),
        [totalQuestions, setTotalQuestions] = useState(0),
        [isLoading, setIsLoading] = useState(true)

  const getSurveys = async () => {
    // Getting the questions from the API
    const response = await APISurvey.getSurveys()
    
    // Saving the questions on the component state to iterate below (return)
    setQuestions(response)

    // We need this number to move forward to the next question. 
    // Also, it helps us to show the total of questions below (return)
    setTotalQuestions(response.length)

    // Saving the original questions to compare them with the user answers.
    store.dispatch(ORG_QUESTIONS(response))

    setTimeout(() => {
      // Turning off the loading
      store.dispatch(IS_LOADING(false))

      // This state variable will help us to show the content when Loading finished
      setIsLoading(false)
    }, 1000)
  }

  // Change questions or Redirect to results page
  const isAnswered = (index) => {
    if(index < totalQuestions){
      setCurrentQuestion(index + 1)
    }else{
      props.history.push('/results')
    }
  }

  useEffect(() => {
    // Setting initial State for original questions
    store.dispatch(ORG_QUESTIONS([]))
    
    // Turning on the loading
    store.dispatch(IS_LOADING(true))

    getSurveys()
  }, [])

  return(
    <section className='page page__survey'>
      { !isLoading ?
        <div className='wrapper'>
          {questions.map((item, index) => 
            <Question
              key={index}
              questionIndex={index+1}
              category={item.category}
              description={renderHTML(item.question)}
              visible={currentQuestion === index+1 ? true : false}
              isAnswered={isAnswered}
            />
          )}
        </div>
      : null }
    </section>
  )
}

export default withRouter(Survey)