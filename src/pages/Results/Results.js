import React, { useState, useEffect } from 'react'
import { NavLink } from 'react-router-dom'

import renderHTML from '../../config/renderHTML'
import store from '../../redux/store'
import { IS_LOADING } from '../../redux/creators'

import ChartResults from '../../components/ChartResults/ChartResults'
import Answer from '../../components/Answer/Answer'

const Results = () => {
  const [questions, setQuestions] = useState([]),
        [isLoading, setIsLoading] = useState(true),
        [showContent, setShowContent] = useState(null),
        [rightAnswers, setRightAnswers] = useState(0)

  const showResults = () => {
    // Getting the original questions from the store.
    const originalQuestions = store.getState().org_questions

    if(originalQuestions.length === 10){
      // Getting the user answers
      const userAnswers = store.getState().answers

      // Mapping the user answers
      const results = userAnswers.map( (item, index) => {
        // The value of correct answer property comes capitalized
        const correctAnswer = originalQuestions[index].correct_answer.toLowerCase()

        // Validating if the user answer is correct
        const isOk = item.answer.toString() === correctAnswer ? true : false

        // Creating items with all the info for each answer
        const newItem = {
          index: index+1,
          user_answer: item.answer,
          correct_answer: correctAnswer,
          is_ok: isOk,
          category: originalQuestions[index].category,
          question: originalQuestions[index].question
        }

        return newItem
      })

      // Setting the correct answers to pass them to chart component
      setRightAnswers(results.filter(item => item.is_ok === true).length)
      
      // Setting question to iterate below (return)
      setQuestions(results)

      setTimeout(() => {
        // Turning off the loading
        store.dispatch(IS_LOADING(false))

        // This state variable will help us to show the content when Loading is finished
        setIsLoading(false)
      }, 1000)
    }
  }
  
  useEffect(() => {
    setShowContent(false)
    if(store.getState().org_questions.length === 10){
      // Show content
      setShowContent(true)

      // Turning on the loading
      store.dispatch(IS_LOADING(true))

      showResults()
    }
  }, [])

  return(
    <section className='page page__results'>
      { showContent === false ? 
        <div className='wrapper no-results'>
          <h2 className='warning-text f-20'>To see the result you must complete all the quiz!</h2>
          <NavLink to='/' className='button button__icon button__arrow is-inline-block'>Begin a quiz</NavLink>
        </div>
      :
        !isLoading ?
        <div className='wrapper'>
          <h2 className='page__title'>Results</h2>
  
          <ChartResults right={rightAnswers} total={questions.length} />
  
          <div className='results__answers'>
            {questions.map( (item, index) =>
              <Answer
                key={index}
                category={item.category}
                question={renderHTML(item.question)}
                is_ok={item.is_ok}
              />
            )}
          </div>
          
          <NavLink to='/' className='results-restart-button button button__icon button__reload is-inline-block'>Play again?</NavLink>
        </div>
        : 
          null
      }
    </section>
  )
}

export default Results