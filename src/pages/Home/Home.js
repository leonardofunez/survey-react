import React from 'react'
import { NavLink} from 'react-router-dom'
import './Home.scss'

const Home = () => {
  return(
    <section className='page-home is-flex flex-justify-center flex-align-center is-absolute h-100 w-100 to-top'>
      <div className='wrapper is-flex flex-align-center flex-between flex-column f-weight-600 align-center'>
        <div className='is-flex flex-column flex-align-center'>
          <div className='logo page-home__logo'></div>
          <h1 className='page-home__title'>Welcome to the Trivia Challenge</h1>
        </div>

        <p className='page-home__subtitle f-18'>You will be presented with 10 True or False questions.</p>
        
        <p className='page-home__challenge-text f-24'>Can you score 100%?</p>
        
        <NavLink className='button button__icon button__arrow is-inline-block' to='/quiz'>Begin</NavLink>
      </div>
    </section>
  )
}

export default Home