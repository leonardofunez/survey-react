export const ORG_QUESTIONS = value => {
  return {
    type: 'ORG_QUESTIONS',
    org_questions: value
  }
}

export const ANSWERS = value => {
  return {
    type: 'ANSWERS',
    answers: value
  }
}

export const IS_LOADING = state => {
  return {
    type: 'IS_LOADING',
    is_loading: state
  }
}