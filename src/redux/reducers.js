// Saving original questions. Probably the questions will change whether we make a new request
export const org_questions = (state = [], action) => {
  if (action.type === 'ORG_QUESTIONS') return action.org_questions

  return state
}

// Saving user answers
export const answers = (state = [], action) => {
  if (action.type === 'ANSWERS') return action.answers

  return state
}

// Show or hide loading component
export const is_loading = (state = false, action) => {
  if (action.type === 'IS_LOADING') return action.is_loading

  return state
}